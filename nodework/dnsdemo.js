const dns = require('dns');
console.log('Resolving www.google.com ...');
dns.resolve4('www.google.com',(error,addresses)=>{
	console.log('IPv4 addresses:'+JSON.stringify(addresses,false,' '));
	addresses.forEach((addr)=>{
		dns.reverse(addr,(err,domains)=>{
			console.log('reverse for '+addr+' domains:'+JSON.stringify(domains,false,' '));
		})
	})
});