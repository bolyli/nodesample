const http = require('http');
const EventEmitter = require('events');
class MyEventEmitter extends EventEmitter{};
const myEventEmitter = new MyEventEmitter();
myEventEmitter.on('event',(data)=>{
	console.log(JSON.stringify(data)+','+JSON.stringify(this));
});
http.createServer((request,response)=>{
	const {method,url,headers} = request;
	let body = [];
	request.on('error',(err)=>{
		console.error(err);
	})
	.on('data',(chunk)=>{
		body.push(chunk);
	})
	.on('end',()=>{
		body = Buffer.concat(body).toString();
	});

	response.setHeader('content-Type','text/html');
	response.writeHeader('200',{
		'sst':'jsldkjflksjdf'
	})
	response.statusCode=200;
	myEventEmitter.emit('event',{'code':200,'message':'success'});
	response.end('<html><head><title>demo</title></head><body>hello world!</body></html>');
}).listen('8807');