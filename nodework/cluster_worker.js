var cluster = require('cluster');
var http = require('http');
if(cluster.isWorker){
	http.createServer(function(req,res){
		res.writeHead(200);
		res.end('process ' + process.pid+' say hello');
		process.send('process '+ process.pid+ ' handle request');
	}).listen(8080,function(){
		console.log('child process open on '+process.pid)
	})
}