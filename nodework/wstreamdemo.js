var fs = require('fs');
var rs = fs.createReadStream('./message.text');
var ws = fs.createWriteStream('./wmessage.text');
var str = '';
rs.on('open',function(){
	console.log('开始读取文件');
});
rs.on('data',function(data){
	console.log('文件读取中');
	str += data;
	ws.write(data);
});
rs.on('end',function(){
	console.log('文件读取完毕');
	console.log(str);
	ws.end('再见',function(){
		console.log('文件写入完毕,共写入%d字节',ws.bytesWritten);
	})
});
rs.on('close',function(){
	console.log('文件关闭');
});
rs.on('error',function(){
	console.log('文件读取失败');
});