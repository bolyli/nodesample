describe('index.js:',function () {
	it('isNum() works fine:',function(){
		expect(isNum(1)).toBe(true);
		expect(isNum('1')).toBe(false);
	})
})