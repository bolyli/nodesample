var fs = require('fs');
var rs = fs.createReadStream('./message.text',{start:3,end:12});
var str = '';
rs.on('open',function(){
	console.log('开始读取文件');
});
rs.on('data',function(data){
	console.log('文件读取中');
	str += data;
});
rs.on('end',function(){
	console.log('文件读取完毕');
	console.log(str);
});
rs.on('close',function(){
	console.log('文件关闭');
});
rs.on('error',function(){
	console.log('文件读取失败');
});
