var spawn = require('child_process').spawn;
var options = {
	stdio:['pipe','pipe','pipe'],
	detached:false,
	encoding:'utf8'
}
var child = spawn('netstat',['-e']);
child.stdout.on('data',function(data){
	console.log(data.toString());
});
child.stderr.on('data',function(data){
	console.log(data.toString())
});
child.on('exit',function(code){
	console.log('child complete with code:'+code);
})