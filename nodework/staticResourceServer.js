const http = require('http');
const fs = require('fs');
const url = require('url');
let server = http.createServer((req,res)=>{
	let urlObj = url.parse(req.url);
	let filename = urlObj.pathname.replace('/','');
	fs.readFile(filename,(err,data)=>{
		if(err){
			res.write('文件读取失败');
			res.end();
		}
		else{
			res.write(data);
			res.end();
		}
	})
});
server.listen(3000);