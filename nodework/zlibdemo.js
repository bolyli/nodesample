const zlib = require('zlib');
const gzip = zlib.createGzip();
const fs = require('fs');
const fsin = fs.createReadStream('input.txt');
const fsout = fs.createWriteStream('input.txt.gz');
fsin.pipe(gzip).pipe(fsout);