var censoredWords = ['sad','mad','dad'];
var customCensoredWords = [];
function censor(inStr){
	for(var i=0;i<censoredWords.length;i++){
		inStr = inStr.replace(censoredWords[i],'****');
	}
	for(var i=0;i<customCensoredWords.length;i++){
		inStr = inStr.replace(customCensoredWords[i],'****');
	}
}
function addCensoredWord(inStr){
	customCensoredWords.push(inStr);

}
function getCensoredWord(){
	return censoredWords.concat(customCensoredWords);
}
exports censor=censor;
exports addCensoredWord = addCensoredWord;
exports getCensoredWord = getCensoredWord;