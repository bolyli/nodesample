var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.post('/',function(req,res,next){
	var rqArr = [];
  req.on('data',function(chunk){
  	rqArr.push(chunk);
  });
  req.on('end',function(){
  	console.log('reqdata :'+Buffer.concat(rqArr).toString())
  })
  res.render('index', { title: 'Express' });
})

module.exports = router;
