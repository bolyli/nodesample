var name='',age=0;
var _name,_age;
var foo = function(name,age){
	_name = name;
	_age = age;
}
foo.prototype.setName = function(name){
	_name = name;
}
foo.prototype.setAge = function(age){
	_age = age;
}
foo.prototype.getName = function(){
	return _name;
}
foo.prototype.getAge = function(){
	return _age;
}
module.exports = foo;